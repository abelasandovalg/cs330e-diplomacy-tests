from Diplomacy import diplomacy_solve

from io import StringIO
from unittest import main, TestCase

class TestDiplomacy(TestCase):

    def test_solve1(self):
        r = StringIO("A ElPaso Hold\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual("A ElPaso\n", w.getvalue())

    def test_solve2(self):
        r = StringIO("A ElPaso Hold\nB Orlando Move ElPaso\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual("A [dead]\nB [dead]\n", w.getvalue())

    def test_solve3(self):
        r = StringIO("C Sydney Support A\nA ElPaso Hold\nB Orlando Move ElPaso\nD London Support B\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual("A [dead]\nB [dead]\nC Sydney\nD London\n", w.getvalue())
    
    def test_solve4(self):
        r = StringIO("A ElPaso Hold\nB Orlando Move ElPaso\nC Sydney Support B\nD London Move Sydney\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual("A [dead]\nB [dead]\nC [dead]\nD [dead]\n", w.getvalue())
        
    def test_solve5(self):
        r = StringIO("A ElPaso Hold\nB Orlando Move ElPaso\nC Sydney Move Orlando\nD London Support A\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual("A ElPaso\nB [dead]\nC Orlando\nD London\n", w.getvalue())
    
    def test_solve6(self):
        r = StringIO("A ElPaso Hold\nB Orlando Move ElPaso\nC Austin Support B\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual("A [dead]\nB ElPaso\nC Austin\n", w.getvalue())
    
    def test_solve7(self):
        r = StringIO("A Portland Move Orlando\nB Orlando Hold\nC Austin Move Portland\nD Detroit Support A\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual("A Orlando\nB [dead]\nC Portland\nD Detroit\n", w.getvalue())

if __name__ == "__main__": #pragma: no cover
    main()
