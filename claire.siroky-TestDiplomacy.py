#!/usr/bin/env python3

# -------------------------------
# projects/diplomacy/TestDiplomacy.py
# Copyright (C) 2016
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.4/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_read, diplomacy_result, diplomacy_print, diplomacy_solve

# -----------
# TestDiplomacy
# -----------


class TestDiplomacy (TestCase):

    # -----
    # read
    # -----
    
    def test_read(self):
        s = "A Madrid Hold\n"
        i = diplomacy_read(s)
        self.assertEqual(i, ["A" , "Madrid", "Hold", 0])
        
    # -----
    # result
    # -----
    
    def test_result(self):
        v = diplomacy_result([["A" , "Madrid", "Hold", 0], ["B" , "Barcelona", "Move", "Madrid"]], [], [])
        self.assertEqual(v,"A [dead]\nB [dead]\n")

    
    # -----
    # solve
    # -----
    
    def test_solve(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\n") 
            

    def test_solve_2(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin Support A\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin\n")
    
  
    def test_solve_3(self):
        r = StringIO("A Madrid Move Austin\nB Austin Support D\nC Houston Hold\nD London Move Houston\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")
    
    def test_solve_4(self):
        r = StringIO("A Madrid Move Houston\nB London Support A\nC Houston Hold\nD Austin Support C\nE Temple Support C\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB London\nC Houston\nD Austin\nE Temple\n")

    def test_solve_5(self):
        r = StringIO("A Madrid Move Houston\nB London Support C\nC Houston Hold\nD Austin Support A\nE Temple Support A\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Houston\nB London\nC [dead]\nD Austin\nE Temple\n")
# ----
# main
# ----
# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestDiplomacy.py >  TestDiplomacy.out 2>&1


$ cat TestDiplomacy.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestDiplomacy.out



$ cat TestDiplomacy.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.002s

OK
Name               Stmts   Miss Branch BrPart  Cover   Missing
--------------------------------------------------------------
Diplomacy.py          68      0     50      0   100%
TestDiplomacy.py      38      0      0      0   100%
--------------------------------------------------------------
TOTAL                106      0     50      0   100%
"""
